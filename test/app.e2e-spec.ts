import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (POST)', () => {
    return request(app.getHttpServer())
      .post('/')
      .send ({
        correo: 'asdd@as.com',
        nombre: 'asdlk',
        nro_doc: '1231231232',
        id_tipo_documento: 1,
        clave: 'aA14857574',
        actual_clave: 'aA14857574',
        id_imagen: null
      })
      .expect(201)
      .expect('{}');
  });
});
