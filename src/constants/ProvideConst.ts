export const SETTINGS_PROVIDER = 'SETTINGS_PROVIDER';

export const DATABASE_CONNECTION = 'DATABASE_CONNECTION';

export const TBL_PERMISSION = 'permission';
export const TBL_PLAN = 'plan';
export const TBL_RESOURCE = 'resource';
export const TBL_RESOURCE_TYPE = 'resource_type';
export const TBL_ROLE = 'role';
export const TBL_ROLE_PERMISSION = 'role_permission';
export const TBL_USER_PERMISSION = 'user_permission';
export const TBL_SESSION = 'session';
export const TBL_USER = 'user';

export const PERMISSION_REPOSITORY = 'PERMISSION_REPOSITORY';
export const PLAN_REPOSITORY = 'PLAN_REPOSITORY';
export const RESOURCE_REPOSITORY = 'RESOURCE_REPOSITORY'; //Verificar correo
export const RESOURCE_TYPE_REPOSITORY = 'RESOURCE_TYPE_REPOSITORY'; //Recuperar contraseña
export const ROLE_REPOSITORY = 'ROLE_REPOSITORY';
export const ROLE_PERMISSION_REPOSITORY = 'ROLE_PERMISSION_REPOSITORY';
export const SESSION_REPOSITORY = 'SESSION_REPOSITORY';
export const USER_REPOSITORY = 'USER_REPOSITORY';


/*export const MAIL_CONNECTION = 'MAIL_CONNECTION';

export const USUARIO_REPOSITORY = 'USUARIO_PROVIDER';
export const TIPO_DOCUMENTO_REPOSITORY = 'TIPO_DOCUMENTO_PROVIDER';
export const TOKEN_VERIFICACION_REPOSITORY = 'TOKEN_VERIFICACION_REPOSITORY'; //Verificar correo
export const TOKEN_RECUPERACION_REPOSITORY = 'TOKEN_RECUPERACION_REPOSITORY'; //Recuperar contraseña
export const ERRORES_CORREO_REPOSITORY = 'ERRORES_CORREO_REPOSITORY';
export const SESION_REPOSITORY = 'SESION_REPOSITORY';
export const ROL_REPOSITORY = 'ROL_REPOSITORY';
export const EMPRESA_REPOSITORY = 'EMPRESA_REPOSITORY';
export const CATEGORIA_REPOSITORY = 'CATEGORIA_REPOSITORY';
export const IMAGEN_REPOSITORY = 'IMAGEN_REPOSITORY';
export const PRODUCTO_REPOSITORY = 'PRODUCTO_REPOSITORY';

export const TBL_USUARIO = 'usuario';
export const TBL_TIPO_DOCUMENTO = 'tipo_documento';
export const TBL_TOKEN_VERIFICACION = 'token_verificacion';
export const TBL_TOKEN_RECUPERACION = 'token_recuperacion';
export const TBL_ERRORES_CORREO = 'errores_correo';
export const TBL_SESION = 'sesion';
export const TBL_ROL = 'rol';
export const TBL_EMPRESA = 'empresa';
export const TBL_CATEGORIA = 'categoria';
export const TBL_IMAGEN = 'imagen';
export const TBL_PRODUCTO = 'producto';
export const TBL_PRODUCTO_IMAGEN = 'producto_imagen';

export const DIR_IMAGES = 'src/files';
*/