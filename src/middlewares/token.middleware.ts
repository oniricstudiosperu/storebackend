import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { AuthenticatorService } from 'src/mods/common/servs/authenticator/authenticator.service';

@Injectable()
export class TokenMiddleware implements NestMiddleware {

  constructor (public authService:AuthenticatorService) { }

  use(req: Request, res: Response, next: Function) {
    this.authService.auth(req);
    next();
  }
}
