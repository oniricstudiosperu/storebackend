import { Connection, Repository } from "typeorm";

export class EntityService<T> {
    repo:Repository<T>;
    constructor (connection:Connection, Model) {
        this.repo = connection.getRepository<T>(Model);
    }
}