export class ExceptionMessage {
    
    public static USER = {
        FORBIDDEN: ['El usuario no existe o no tienes permisos para realizar la acción', 1001],
        NOT_FOUND: ['El usuario no existe', 1002]
    };
    
    public static INPUT = {
        REQUIRED: ['El índice $1 es requerido', 2001],
        NOT_NULL: ['El índice $1 no puede ser null', 2001],
        NOT_EMPTY: ['El índice $1 no puede ser estar vacío', 2001],
        EMAIL: ['El índice $1 debe ser un email', 2001],
        IN_DATABASE: ['El índice $1 no existe', 2001],
        UNIQUE: ['Ya existe un elemento con igual $1', 2001],
        STRING: ['El índice $1 debe ser un string', 2001],
        NUMBER: ['El índice $1 debe ser un número', 2001],
        INT: ['El índice $1 no es un entero', 2001],
        MAX_LENGTH: ['El índice $1 no puede tener más de $2 caracteres', 2001],
        MIN_LENGTH: ['El índice $1 no puede tener menos de $2 caracteres', 2001],
        UNLIKE_PASSWORD: ['El índice $1 no es igual a la clave actual', 2002],
        NOT_SECURE_PASSWORD: ['El índice $1 debe tener al menos: un dígito, una minúscula, una mayúscula y entre $2 y $3 caracteres', 2003],
        DATE: ['El índice $1 debe ser una fecha válida', 2001],
        ONLY_NUMBERS: ['El índice $1 sólo puede contener números', 2001],
        ONLY_IMAGES: ['El archivo $1 sólo puede contener imágenes con extensión $2', 2001],
    };

    public static RESOURCE_NOT_FOUND = ['El recurso que estás buscando no existe o no tienes permisos para verlo', 3001];
}