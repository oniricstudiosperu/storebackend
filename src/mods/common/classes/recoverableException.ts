export class RecoverableException {
    
    
    static codeBadRequest:number = 400;
    static codeUnauthorized:number = 401;
    static codePaymentRequired:number = 402;
    static codeForbidden:number = 403;
    
    public name:string = '';
    constructor (
        public message:string,
        public code:number,
        public statusCode:number
    ) { }
}