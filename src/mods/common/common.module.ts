import { Module } from '@nestjs/common';
import { AuthenticatorService } from './servs/authenticator/authenticator.service';
import { SessionService } from './servs/session/session.service';
import { DatabaseModule } from '../database/database.module';
import { ResponsesService } from './servs/responses/responses.service';
import { UserService } from './servs/user/user.service';
import { UserPermissionService } from './servs/user-permission/user-permission.service';

@Module({
  exports: [ AuthenticatorService, SessionService, ResponsesService, UserService, UserPermissionService ],
  providers: [AuthenticatorService, SessionService, ResponsesService, UserService, UserPermissionService],
  imports: [
    DatabaseModule
  ]
})
export class CommonModule {}
