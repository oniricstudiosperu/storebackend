import { Test, TestingModule } from '@nestjs/testing';
import { ResponsesService } from './responses.service';

describe('ResponsesService', () => {
  let service: ResponsesService;
  let module: TestingModule;
  beforeEach(async () => {
    module = await Test.createTestingModule({
      providers: [ResponsesService],
    }).compile();

    service = module.get<ResponsesService>(ResponsesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  afterAll(async () => {
    await module.close();
  });
});
