import { Injectable, HttpStatus } from '@nestjs/common';
import { RecoverableException } from '../../classes/recoverableException';

@Injectable()
export class ResponsesService {
    throwNotFound (arr: Array<string|number>, ...change) {
        throw new RecoverableException (<string>arr[0], <number>arr[1], HttpStatus.NOT_FOUND);
    }

    throwForbidden (arr: Array<string|number>, ...change) {
        throw new RecoverableException (<string>arr[0], <number>arr[1], HttpStatus.FORBIDDEN);
    }

    throwBadRequest (arr: Array<string|number>, ...change) {
        let str = arr[0];
        for (let i in change) {
            str = (str as string).replace ('$' + (parseInt(i) - (-1)), change[i]);
        }
        throw new RecoverableException (<string>str, <number>arr[1], HttpStatus.BAD_REQUEST);
    }
}
