import { Injectable, Inject } from '@nestjs/common';
import { EntityService } from 'src/classes/EntityService';
import { User } from 'src/mods/database/models/user.entity';
import { Connection } from 'typeorm';
import { DATABASE_CONNECTION } from 'src/constants/ProvideConst';

@Injectable()
export class UserService extends EntityService<User> {
    constructor (
        @Inject(DATABASE_CONNECTION) public readonly connection,
    ) {
        super(connection, User);
    }
}
