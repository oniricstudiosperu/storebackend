import { Injectable, Inject } from '@nestjs/common';
import { EntityService } from 'src/classes/EntityService';
import { DATABASE_CONNECTION } from 'src/constants/ProvideConst';
import { UserPermission } from 'src/mods/database/models/userPermission.entity';
import { User } from 'src/mods/database/models/user.entity';
import { Not } from 'typeorm';

@Injectable()
export class UserPermissionService extends EntityService<UserPermission> {
    constructor (
        @Inject(DATABASE_CONNECTION) public readonly connection,
    ) {
        super(connection, UserPermission);
    }

    async requireUpdatedPermissions (user:User) {
        let sd = 'startdate_user_permission';
        let ed = 'enddate_user_permission';
        let curr = new Date();
        let active = true;
        let permissions = await this.repo
            .createQueryBuilder("*")
            .where(`((${sd} is null or ${ed} is null) or sd<:curr and ed>:curr) and active=:active`, { curr, active })
            .execute();
        console.log (permissions);
        return permissions;
    }
}
