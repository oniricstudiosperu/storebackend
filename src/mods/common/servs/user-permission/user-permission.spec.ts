import { Test, TestingModule } from '@nestjs/testing';
import { DatabaseModule } from 'src/mods/database/database.module';
import { UserPermissionService } from './user-permission.service';

describe('UserService', () => {
  let service: UserPermissionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserPermissionService],
      imports: [DatabaseModule]
    }).compile();

    service = module.get<UserPermissionService>(UserPermissionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
