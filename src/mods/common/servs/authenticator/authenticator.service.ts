import { Injectable, Inject } from '@nestjs/common';
import { Session } from 'src/mods/database/models/session.entity';
import { SETTINGS_PROVIDER } from 'src/constants/ProvideConst';
import { SessionService } from '../session/session.service';
import { RecoverableException } from '../../classes/recoverableException';
import { UserService } from 'src/mods/common/servs/user/user.service';
import { UserPermissionService } from 'src/mods/common/servs/user-permission/user-permission.service';
const jwt = require ('jsonwebtoken');

@Injectable()
export class AuthenticatorService {
    public user;
    public role;
    public session;
    public status;
    public permissions = [];

    public constructor (
        @Inject(SETTINGS_PROVIDER) private readonly settings,
        private sessionService:SessionService,
        private userService:UserService,
        private userPermissionService:UserPermissionService
    ) { }

    public onStart () {
        //session_start();
    }

    protected allNull() {
        this.user = null;
        this.role = null;
        this.session = null;
    }

    protected setEmpty () {
        this.allNull();
        this.status = 'Empty';
    }
    protected setFailed() {
        this.allNull();
        this.status = 'Failed';
    }
    protected setOutOfTime () {
        this.allNull();
        this.status = 'OutOfTime';
    }
    
    /**
     * auth
     */
    public async auth (req) {
        this.setEmpty();

        if (!req.headers ['access_token']) { return; }

        if (!req.headers ['uid']) { return; }
        
        try {
            let accessToken = jwt.verify (req.headers.access_token, this.settings.jwtKey);
            
            
            if (!accessToken.uid
                || accessToken.uid != req.headers.uid
                || !accessToken.session) {
                return this.setFailed();
            }

            let model = new Session();
            let session = await this.sessionService.repo.findOne ({
                id_session: accessToken.session
            });

            if (!session) {
                return this.setFailed();
            }

            let now = new Date();
            if (session.start_date_session > now || session.end_date_session < now || !session.active_session) {
                return this.setOutOfTime();
            }

            session.end_date_session = new Date(Math.floor(Date.now() / 1000) + (30 * 60));
            session.used_date_session = new Date();
            session.save();
            
            this.user = await this.userService.repo.findOne (session.id_user);
            this.permissions = await this.userPermissionService.requireUpdatedPermissions(this.user); 
            
            //this.role = $db.getManager().findOne ($roleModel, (object) [
            //    'where' => [ $userModel.getIdName() => $session.id_user ]
            //]);

            //this.permissions = this.role.findRelation ('permissions');
            //this.status = 'Success';
            
        } catch (ex) {
            this.setFailed();
        }
        return;
    }
    public static $time = "1800"; //tiempo en segundos

    public requireUser () {
        if (this.status != 'Success') {
            throw new RecoverableException ('Usuario no logueado', 401, 401);
        }
    }
    public requirePermission (...$permissionName) {
        this.requireUser();
        if (!this.inPermission (...$permissionName)) {
            throw new RecoverableException ('El usuario no tiene permisos para realizar la acción', 403, 403);
        }
        return this;
    }
    public inPermission (...permissionName) {
        let flag = false;
        for (let i in permissionName) {
            for (let j in this.permissions) {
                if (this.permissions[j].name == permissionName[i]) {
                    flag = true;
                }
            } 
            
        }
        return flag;
    }
}
