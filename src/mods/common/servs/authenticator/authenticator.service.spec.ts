import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticatorService } from './authenticator.service';
import { DatabaseModule } from 'src/mods/database/database.module';
import { SessionService } from '../session/session.service';
import { UserService } from 'src/mods/common/servs/user/user.service';
import { UserPermissionService } from '../user-permission/user-permission.service';

describe('AuthenticatorService', () => {
  let service: AuthenticatorService;
  let module: TestingModule;
  beforeEach(async () => {
    module = await Test.createTestingModule({
      providers: [AuthenticatorService, SessionService, UserService, UserPermissionService],
      imports: [DatabaseModule]
    }).compile();

    service = module.get<AuthenticatorService>(AuthenticatorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  afterAll(async () => {
    await module.close();
  });
});
