import { Test, TestingModule } from '@nestjs/testing';
import { SessionService } from './session.service';
import { DatabaseModule } from 'src/mods/database/database.module';
import { CommonModule } from '../../common.module';

describe('SessionService', () => {
  let service: SessionService;
  let module: TestingModule;
  beforeEach(async () => {
    module = await Test.createTestingModule({
      providers: [SessionService],
      imports: [DatabaseModule, CommonModule]
    }).compile();

    service = module.get<SessionService>(SessionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  afterAll(async () => {
    await module.close();
  });
});
