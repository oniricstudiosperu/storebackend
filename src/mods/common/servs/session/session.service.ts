import { Injectable, Inject } from '@nestjs/common';
import { SESSION_REPOSITORY } from 'src/constants/ProvideConst';
import { Session } from 'src/mods/database/models/session.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SessionService {
    constructor(
        @Inject(SESSION_REPOSITORY)
        public readonly repo: Repository<Session>
    ) {}
}
