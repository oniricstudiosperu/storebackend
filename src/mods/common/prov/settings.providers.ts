import { SETTINGS_PROVIDER } from "src/constants/ProvideConst";

export const settingsProviders = [
    {
      provide: SETTINGS_PROVIDER,
      useFactory: () => ({
        jwtKey: '1874a98ds7aaasdasdasdaq232131sdasd84asd65as4',
        host: 'http://localhost:3000',
        sessionExpireTime: 60 * 60, //in seconds
        recoverExpireTime: 30 * 60, //in seconds
        verifyExpireTime: 2 * 24 * 60 * 60, //in seconds
        verifiedUrl: '/mailverified',
        unverifiedUrl: '/mailunverified',
      }),
    },
  ];