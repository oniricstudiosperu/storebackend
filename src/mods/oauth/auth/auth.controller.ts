import { Controller, Get, Post, Body, Inject } from '@nestjs/common';
import { AuthenticatorService } from 'src/mods/common/servs/authenticator/authenticator.service';
import { UserService } from '../../common/servs/user/user.service';
import { ResponsesService } from 'src/mods/common/servs/responses/responses.service';
import { ExceptionMessage } from 'src/classes/ExceptionMessage';
import { Session } from 'src/mods/database/models/session.entity';
import { SETTINGS_PROVIDER } from 'src/constants/ProvideConst';
import { SessionService } from 'src/mods/common/servs/session/session.service';
import { Not } from 'typeorm';
const jwt = require ('jsonwebtoken');

@Controller('auth')
export class AuthController {
    message:any;
    isUniqueSession:boolean = true;
    constructor (
        private readonly authService:AuthenticatorService,
        private readonly userService:UserService,
        private readonly responses:ResponsesService,
        @Inject(SETTINGS_PROVIDER) public readonly settings,
        private readonly sessionService:SessionService,
        ) { }
    @Get()
    public _get () {
        this.authService.requireUser();

		return {
            user: this.authService.user,
			role: this.authService.role,
			permissions: this.authService.permissions
        };
    }

    @Post()
    public async _post (@Body() body) {
        //this.message = this.responses.initAccessToken();
        

        //this.inputService.setInput (body);
        let user = null;

		//this.inputService
		//	.rules ( 'account', InType::Required, InType::NotNull, InType::NotEmpty )
        //	.rules ( 'password', InType::Required, InType::NotNull, InType::NotEmpty );
        user = await this.userService.repo.findOne ({ account_user: body.account_user });
        
        if (!user) {
			this.responses.throwNotFound(ExceptionMessage.USER.NOT_FOUND);
		}
		//this.inputService.rules ('password', [InType::ComparePassword, user]);

        let sessionModel = new Session();
		sessionModel.id_user = user.id_user;
		sessionModel.jwt_session = '';
		sessionModel.end_date_session = new Date (Date.now() + 30 * 60);
		sessionModel.used_date_session = new Date();
		sessionModel.start_date_session = new Date();
        sessionModel.active_session = true;
        sessionModel = await sessionModel.save();
		let payload = {
			//'iat' => this.app.requestTime,
			//'nbf' => this.app.requestTime + 30 * 60,
			'session': sessionModel.id_session,
			'uid': user.id_user
        };
        console.log (this.settings.jwtKey);

		let jwtToken = jwt.sign (payload, this.settings.jwtKey);
		
		sessionModel.jwt_session = jwtToken;

        await this.sessionService.repo.update (sessionModel.id_session, sessionModel);
		if (this.isUniqueSession) {
            await this.sessionService.repo.update (
                {
                    active_session: true,
                    id_session: Not (sessionModel.id_session),
                    id_user: user.id_user
                },
                { active_session: false }
            );
			
		}
		return {
			access_token: sessionModel.jwt_session,
			uid: sessionModel.id_user
        };
    }
}
