import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { CommonModule } from 'src/mods/common/common.module';
import { DatabaseModule } from 'src/mods/database/database.module';
import { async } from 'rxjs/internal/scheduler/async';
import { UserService } from '../../common/servs/user/user.service';

describe('Auth Controller', () => {
  let controller: AuthController;
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      providers: [UserService],
      controllers: [AuthController],
      imports: [CommonModule, DatabaseModule],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a token', async () => {
    console.log (await controller._post ({
      account_user: 'admin',
      password_user: '12345678'
    }));
  });
  afterAll(async () => {
    await module.close();
  });
});
