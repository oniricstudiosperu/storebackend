import { Module } from '@nestjs/common';
import { AuthController } from './auth/auth.controller';
import { CommonModule } from '../common/common.module';
import { DatabaseModule } from '../database/database.module';
import { UserService } from '../common/servs/user/user.service';

@Module({
  imports: [CommonModule, DatabaseModule],
  controllers: [AuthController],
  providers: []
})
export class OauthModule {}
