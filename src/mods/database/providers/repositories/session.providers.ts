import { Connection, Repository } from 'typeorm';
import { SESSION_REPOSITORY, DATABASE_CONNECTION } from 'src/constants/ProvideConst';
import { Session } from '../../models/session.entity';

export const sessionProviders = [ {
    provide: SESSION_REPOSITORY,
    useFactory: (connection: Connection) => connection.getRepository(Session),
    inject: [ DATABASE_CONNECTION ]
  }
];