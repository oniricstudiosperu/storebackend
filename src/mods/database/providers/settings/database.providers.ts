import { createConnection } from 'typeorm';
import { DATABASE_CONNECTION } from 'src/constants/ProvideConst';

export const databaseProviders = [
  {
    provide: DATABASE_CONNECTION,
    useFactory: async () => await createConnection({
      type: 'mysql',
      host: 'localhost',
      port:   3306,
      username: 'root',
      password: '',
      database: 'store',
      entities: [
          __dirname + '/../../models/*.entity{.ts,.js}',
      ],
      synchronize: false,
    }),
  },
];