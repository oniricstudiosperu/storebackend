import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { TBL_PLAN } from 'src/constants/ProvideConst';

@Entity(TBL_PLAN)
export class Plan extends BaseEntity {
  constructor() {
      super();
  }
  @PrimaryGeneratedColumn() id_plan:number;

  @Column() name_plan:string;

  @Column() description_plan:string;

  @Column() visible_plan:boolean;

  @Column() active_plan:boolean;

  @Column() position_plan:number;

  @Column() creation_date_plan:Date;

  @Column() price_plan:number;
  
}