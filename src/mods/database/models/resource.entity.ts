import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { TBL_RESOURCE } from 'src/constants/ProvideConst';

@Entity(TBL_RESOURCE)
export class Resource extends BaseEntity {
  constructor() {
      super();
  }
  @PrimaryGeneratedColumn() id_resource:number;

  @Column() id_user:number;

  @Column() id_image:number;

  @Column() name_resource:string;

  @Column() description_resource:string;

  @Column() id_node_type_resource:number;

  @Column() position_resource:number;

  @Column() creation_date_resource:Date;

  @Column() modified_date_resource:Date;

  @Column() active_resource:boolean;

  @Column() visible_resource:boolean;
  
}