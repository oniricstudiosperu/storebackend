import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { TBL_SESSION } from 'src/constants/ProvideConst';

@Entity(TBL_SESSION)
export class Session extends BaseEntity {
  constructor() {
      super();
  }
  @PrimaryGeneratedColumn() id_session:number;

  @Column() id_user:number;

  @Column() jwt_session:string;

  @Column() start_date_session:Date;

  @Column() end_date_session:Date;

  @Column() used_date_session:Date;

  @Column() active_session:boolean;
  
}