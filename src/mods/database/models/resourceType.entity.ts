import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { TBL_RESOURCE_TYPE } from 'src/constants/ProvideConst';

@Entity(TBL_RESOURCE_TYPE)
export class ResourceType extends BaseEntity {
  constructor() {
      super();
  }
  @PrimaryGeneratedColumn() id_resource_type:number;

  @Column() typename_resource_type:string;

  @Column() tablename_resource_type:string;
  
}