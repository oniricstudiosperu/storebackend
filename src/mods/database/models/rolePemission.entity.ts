import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { TBL_ROLE_PERMISSION } from 'src/constants/ProvideConst';

@Entity(TBL_ROLE_PERMISSION)
export class RolePermission extends BaseEntity {
  constructor() {
      super();
  }
  @PrimaryGeneratedColumn() id_role_permission:number;

  @Column() id_role:number;

  @Column() id_permission:number;
  
}