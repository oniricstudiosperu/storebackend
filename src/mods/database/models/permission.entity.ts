import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { TBL_PERMISSION } from 'src/constants/ProvideConst';

@Entity(TBL_PERMISSION)
export class Permission extends BaseEntity {
  constructor() {
      super();
  }
  @PrimaryGeneratedColumn() id_permission:number;

  @Column({ length: 250 }) name_permission:string;

  @Column() description_permission:string;
  
}