import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { TBL_ROLE } from 'src/constants/ProvideConst';

@Entity(TBL_ROLE)
export class Role extends BaseEntity {
  constructor() {
      super();
  }
  @PrimaryGeneratedColumn() id_role:number;

  @Column() name_role:string;

  @Column() description_role:string;
  
}