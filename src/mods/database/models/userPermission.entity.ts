import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { TBL_USER_PERMISSION } from 'src/constants/ProvideConst';

@Entity(TBL_USER_PERMISSION)
export class UserPermission extends BaseEntity {
  constructor() {
      super();
  }
  @PrimaryGeneratedColumn() id_user_permission:number;

  @Column() id_user:number;

  @Column() id_permission:number;
  
  @Column() startdate_user_permission:Date;
  
  @Column() enddate_user_permission:Date;
  
  @Column() active_user_permission:boolean;
  
}