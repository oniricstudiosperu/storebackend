import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { TBL_USER } from 'src/constants/ProvideConst';

@Entity(TBL_USER)
export class User extends BaseEntity {
  constructor() {
      super();
  }
  @PrimaryGeneratedColumn() id_user:number;

  @Column() id_role:number;

  @Column() account_user:string;

  @Column() password_user:string;

  @Column() email_user:string;

  @Column() phone_user:string;

  @Column() active_user:boolean;

  @Column() creation_date_user:Date;
  
}