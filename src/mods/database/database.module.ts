import { Module } from '@nestjs/common';
import { sessionProviders } from './providers/repositories/session.providers';
import { databaseProviders } from './providers/settings/database.providers';
import { settingsProviders } from '../common/prov/settings.providers';

@Module({
    providers: [
        ...sessionProviders,
        ...databaseProviders,
        ...settingsProviders,
    ],
    exports: [
        ...sessionProviders,
        ...databaseProviders,
        ...settingsProviders,
    ]
})
export class DatabaseModule {}
