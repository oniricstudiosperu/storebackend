import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TokenMiddleware } from './middlewares/token.middleware';
import { OauthModule } from './mods/oauth/oauth.module';
import { CommonModule } from './mods/common/common.module';
import { DatabaseModule } from './mods/database/database.module';

@Module({
  imports: [OauthModule, CommonModule, DatabaseModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(TokenMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
